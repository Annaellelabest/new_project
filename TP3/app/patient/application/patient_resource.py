from flask import request, jsonify, Blueprint
from json import JSONEncoder

from flask import current_app as app
from jose import jwt
from flask import Flask
from flask import request, jsonify, Blueprint
from flask_oidc import OpenIDConnect
from patient.infrastructure.patient_entity import PatientEntity
import json


oidc_patient = OpenIDConnect()


patient_bp = Blueprint('patient', __name__)
#patient_bp = Blueprint('patient', __name__)

#app = Flask(__name__)

#create un patient
@patient_bp.route('/patients', methods=['POST'])
def create_patient():
    try:
        patient_data = request.get_json()
        patient_service = app.config['patient_service']
        created_patient = patient_service.create_patient(Patient(**patient_data))

        return jsonify(created_patient.to_dict()), 201
    except Exception as e:
        return jsonify({'error': str(e)}), 400
    
#affiche tous les patients
# @patient_bp.route('/patients', methods=['GET'])
# def get_patients():
#     try:
#         patient_service = app.config['patient_service']
#         patients = patient_service.get_patients()
#         return jsonify([patient.to_dict() for patient in patients]), 200
#     except Exception as e:
#         return jsonify({'error': str(e)}), 400
    

#affiche le patient en rentrant son id
@patient_bp.route('/patients/<patient_id>', methods=['GET'])
def get_patient(patient_id):
    try:
        patient_service = app.config['patient_service']
        patient = patient_service.get_patient(patient_id)
        return jsonify(patient.to_dict()), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 400

#affiche le patient en rentrant son ssn
@patient_bp.route('/patients', methods=['GET'])
def get_patient_by_ssn():
    try:
        ssn = request.args.get('ssn')
        if not ssn:
            return jsonify({'error': 'SSN parameter is required'}), 400
        patient_service = app.config['patient_service']
        patient = patient_service.get_patient_by_ssn(ssn)
    except Exception as e:
        return jsonify({'error': str(e)}), 400

#affiche le patient en rentrant son nom
@patient_bp.route('/patients', methods=['GET'])
def get_patient_by_name():
    try:
        name = request.args.get('name')
        if not name:
            return jsonify({'error': 'Name parameter is required'}), 400
        patient_service = app.config['patient_service']
        patient = patient_service.get_patient_by_name(name)
    except Exception as e:
        return jsonify({'error': str(e)}), 400

#commenter cette partie car sinon il y a une erreur car il y a deux fonctions qui ont le meme nom
#mais elle affiche les patients  sans avoir besoin le role

# @patient_bp.route('/patients', methods=['GET'])
# def get_patient_by_surname():
#     try:
#         surname = request.args.get('surname')
#         if not surname:
#             return jsonify({'error': 'Surname parameter is required'}), 400
#         patient_service = app.config['patient_service']
#         patient = patient_service.get_patient_by_surname(surname)
#     except Exception as e:
#         return jsonify({'error': str(e)}), 400

#update un patient
    
@patient_bp.route('/patients/<int:patient_id>', methods=['PUT'])
def update_patient(patient_id):
    try:
        patient_data = request.get_json()
        patient_service = app.config['patient_service']
        patient = patient_service.update_patient(patient_id, Patient(**patient_data))
        if not patient:
            return jsonify({'error': 'Patient not found'}), 404
        
        return jsonify(patient.to_dict())
    except Exception as e:
        return jsonify({'error': str(e)}), 400

#delete un patient
    
@patient_bp.route('/patients/<int:patient_id>', methods=['DELETE'])
def delete_patient(patient_id):
    try:
        patient_service = app.config['patient_service']
        patient = patient_service.delete_patient(patient_id)
        if not patient:
            return jsonify({'error': 'Patient not found'}), 404
        return jsonify(patient.to_dict())
    except Exception as e:
        return jsonify({'error': str(e)}), 400



# pouvoir faire afficher les patients que si tu as le role doctor
@patient_bp.route('/doctor', methods=['GET'])
@oidc_patient.accept_token()

def get_patients():
        """OAuth 2.0 protected API endpoint accessible via AccessToken"""
        token = request.headers.get('Authorization').split(' ')[1]
        claim = jwt.get_unverified_claims(token)
        roles = claim.get('realm_access', {}).get('roles', [])
        if 'doctor' not in roles:
            abort(403, 'Access forbidden. Only users in the "doctor" group can access this endpoint.')
            patient_service = app.config['patient_service']
            patients = patient_service.get_patients()
            
            patients = patient_service.create_patient(new_patient)
            return json.dumps(patients)



@patient_bp.route('/doctor', methods=['POST'])
#@oidc.accept_token()
def create_patient():
        """OAuth 2.0 protected API endpoint accessible via AccessToken"""
        token = request.headers.get('Authorization').split(' ')[1]
        claim = jwt.get_unverified_claims(token)
        roles = claim.get('realm_access', {}).get('roles', [])
        if 'doctor' not in roles:
            abort(403, 'Access forbidden. Only users in the "doctor" group can access this endpoint.')
        patient_data = request.get_json()

        new_patient = PatientEntity(
        first_name=patient_data.get('first_name'),
        last_name=patient_data.get('last_name'),
        date_of_birth=patient_data.get('date_of_birth'),
        social_security_number=patient_data.get('social_security_number'),
        id=1

    )

        patient_service = app.config['patient_service']
        patients = patient_service.create_patient(new_patient)
        
        return json.dumps(patients)




