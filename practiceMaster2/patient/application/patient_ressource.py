from flask import request, jsonify, Blueprint
from flask import current_app as app


from patient.domain.patient import Patient

patient_bp = Blueprint('patient', __name__)


@patient_bp.route('/patients', methods=['POST'])
def create_patient():
    try:
        patient_data = request.get_json()
        patient_service = app.config['patient_service']
        created_patient = patient_service.create_patient(Patient(**patient_data))

        return jsonify(created_patient.to_dict()), 201
    except Exception as e:
        return jsonify({'error': str(e)}), 400

@patient_bp.route('/patients/<int:patient_id>', methods=['GET'])
def search_patient(patient_id):
    try:
        patient_service = app.config['patient_service']
        patient = patient_service.get_patient(patient_id)

        return jsonify(patient.to_dict()), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 400

@patient_bp.route('/patients/<int:patient_id>', methods=['DELETE'])
def delete_patient(patient_id):
    try:
        patient_service = app.config['patient_service']
        patient = patient_service.delete_patient(patient_id)

        return jsonify(patient.to_dict()), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 400

@patient_bp.route('/patients/<int:patient_id>', methods=['PUT'])
def update_patient(patient_id):
    try:
        patient_data = request.get_json()
        patient_service = app.config['patient_service']
        patient = patient_service.update_patient(patient_id, Patient(**patient_data))

        return jsonify(patient.to_dict()), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 400

@patient_bp.route('/patients', methods=['GET'])
def get_all_patients():
    try:
        patient_service = app.config['patient_service']
        patients = patient_service.get_all_patients()

        return jsonify([patient.to_dict() for patient in patients]), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 400



